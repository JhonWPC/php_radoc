<nav class="container mt-5 p-4 border rouded-3 shadow-sm bg-light">
    <div class="row align-items-center">

        <div class="col">
            <h4>Aplicação PHP</h4>
        </div>

        <div class="col text-center">
            <a href="?rota=home">Home</a>
            <span class="mx-2">|</span>
            <a href="?rota=page1">Cadastro</a>
            <span class="mx-2">|</span>
            <a href="?rota=page2">Página do Docente</a>
            <span class="mx-2">|</span>
            <a href="?rota=page3">Página CPPD</a>
        </div>

        <div class="col text-end">
            <span>Usuário logado: <strong><?= $_SESSION['usuario']->usuario ?></strong></span>
            <span class="mx-2">|</span>
            <a href="?rota=logout">Sair</a>
        </div>

    </div>
</nav>